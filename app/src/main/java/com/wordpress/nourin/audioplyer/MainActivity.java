package com.wordpress.nourin.audioplyer;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    ImageButton ibPaly,ibPause,ibStop;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ibPaly = (ImageButton)findViewById(R.id.ib_activity_main_play);
        ibPause = (ImageButton)findViewById(R.id.ib_activity_main_pause);
        ibStop = (ImageButton)findViewById(R.id.ib_activity_main_stop);

        ibPaly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mediaPlayer==null){
                    mediaPlayer = MediaPlayer.create(getApplicationContext(),R.raw.audio);
                }

                mediaPlayer.start();

            }
        });

        ibPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer!=null){
                    mediaPlayer.pause();
                }
            }
        });


        ibStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mediaPlayer!=null){
                    mediaPlayer.release();
                    mediaPlayer = null;

                }

            }
        });


    }
}
